#include <chrono>
#include <iostream>
#include "fonctions.h"

using namespace std;

int main() {

    const int N_SOMME = 3;
    const int A_PUISSANCE = 2;
    const int N_PUISSANCE = 2;
    const int A_PGCD = 10;
    const int B_PGCD = 5;

    cout << "Recursif : " << endl << endl;

    cout << "Somme ";

    for (int i = 0; i <= N_SOMME; i++) {

        cout << i;
        if (i != N_SOMME) {
            cout << "+";
        }
    }

    cout << " = " << somme_Recurante(N_SOMME) << endl << endl;


    cout << A_PUISSANCE << " puissance " << N_PUISSANCE << " = " << puissanceN_Recurante(A_PUISSANCE, N_PUISSANCE)
         << endl << endl;

    auto start1 = chrono::steady_clock::now();

    cout << "PGCD entre " << A_PGCD << " et " << B_PGCD << " = " << PGCD_Recurant(A_PGCD, B_PGCD) << endl;

    auto end1 = chrono::steady_clock::now();

    cout << "==> Temps d'execution : " << chrono::duration<double, milli>(end1 - start1).count() << " ms <==" << endl
         << endl;

    cout << endl << "- - - - - - - - -" << endl << endl;

    cout << "Non Recursif : " << endl << endl;

    cout << "Somme ";

    for (int i = 0; i <= N_SOMME; i++) {

        cout << i;
        if (i != N_SOMME) {
            cout << "+";
        }
    }

    cout << " = " << somme(N_SOMME) << endl << endl;


    cout << A_PUISSANCE << " puissance " << N_PUISSANCE << " = " << puissanceN(A_PUISSANCE, N_PUISSANCE) << endl
         << endl;

    auto start2 = chrono::steady_clock::now();

    cout << "PGCD entre " << A_PGCD << " et " << B_PGCD << " = " << PGCD(A_PGCD, B_PGCD) << endl;

    auto end2 = chrono::steady_clock::now();

    cout << "==> Temps d'execution : " << chrono::duration<double, milli>(end2 - start2).count() << " ms <==" << endl
         << endl;


    return 0;
}