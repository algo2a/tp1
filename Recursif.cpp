//
// Created by Arnaud on 21/09/2018.
//

///////////////////
//// Récursif  ////
///////////////////

///////////////////
/*
 * Cette fonction calcul de manière récurante la somme des N entier consécutif et renvoie le résultat.
 * Retourne -1 si l'entier N est invalid
 *
 * ENTRE : l'entier N
 * SORTIE : rien
 */
///////////////////
int somme_Recurante(int n) {
    if (n >= 0) {
        if (n == 0) { //Condition d'arret
            return 0;
        } else {
            return somme_Recurante(n - 1) + n; //Rappel de la fonction somme_Recurante
        }
    } else {
        return -1;
    }
}


///////////////////
/*
 * Cette fonction calcul de manière récurante la puissance de A exposant N
 * Retourne -1 si l'entier N est négatif
 *
 * ENTRE : l'entier A, l'entier N
 * SORTIE : rien
 */
///////////////////
long long int puissanceN_Recurante(int a, int n) {

    long long int temp;

    if (n < 0) {
        return -1;
    } else {

        if (n == 0) { //Condition d'arret
            return 1;
        } else if (n == 1) { //Condition d'arret
            return a;
        } else {

            if (n % 2 == 0) {
                temp = puissanceN_Recurante(a, n / 2); //Rappel de la fonction puissanceN_Recurante
                return temp * temp;
            } else {
                temp = puissanceN_Recurante(a, (n - 1) / 2); //Rappel de la fonction puissanceN_Recurante
                return a * temp * temp;
            }
        }
    }
}


///////////////////
/*
 * Cette fonction calcul de manière récurante le PGCD entre A et B
 * Retourne -1 si l'entier A et/ou l'entier B sont/est négatif ou nul
 *
 * ENTRE : l'entier A, l'entier B
 * SORTIE : rien
 */
///////////////////
int PGCD_Recurant(int a, int b) {

    if (a > 0 && b > 0) {
        if (a == b) {
            return a; //Condition d'arret
        } else {
            if (a > b) {
                return PGCD_Recurant(a - b, b); //Rappel de la fonction PGCD_Recurant
            } else {
                return PGCD_Recurant(a, b - a); //Rappel de la fonction PGCD_Recurant
            }
        }
    } else {
        return -1;
    }
}