//
// Created by Arnaud on 21/09/2018.
//

///////////////////////
////   Iteratif    ////
///////////////////////


///////////////////
/*
 * Cette fonction calcul de manière itérative la somme des N entier consécutif et renvoie le résultat
 * Retourne -1 si l'entier N est invalid
 *
 * ENTRE : l'entier N
 * SORTIE : rien
 */
///////////////////

int somme(int n) {

    int somme = 0;

    if (n >= 0) {
        for (int i = 0; i <= n; i++) {
            somme += i;
        }
    } else {
        somme = -1; //Code retour si entier N négatif
    }

    return somme;
}


///////////////////
/*
 * Cette fonction calcul de manière itérative la puissance de A exposant N
 * Retourne -1 si l'entier N est négatif
 *
 * ENTRE : l'entier A, l'entier N
 * SORTIE : rien
 */
///////////////////
long long int puissanceN(int a, int n) {

    long long int puissance;

    if (n < 0) {
        puissance = -1;
    } else {

        if (n == 0) {
            puissance = 1;
        } else {

            puissance = a;

            for (int i = 1; i < n; i++) {
                puissance *= a;
            }
        }
    }

    return puissance;
}


///////////////////
/*
 * Cette fonction calcul de manière itérative le PGCD entre A et B
 * Retourne -1 si l'entier A et/ou l'entier B sont/est négatif ou nul
 *
 * ENTRE : l'entier A, l'entier B
 * SORTIE : rien
 */
///////////////////
int PGCD(int a, int b) {

    if (a > 0 && b > 0) {
        while (a != b) {
            if (a > b) {
                a -= b;
            } else {
                b -= a;
            }
        }
        return a;
    } else {
        return -1;
    }
}