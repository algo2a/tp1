//
// Created by Arnaud on 21/09/2018.
//

#ifndef RECURSIVITE_FONCTIONS_H
#define RECURSIVITE_FONCTIONS_H

int somme_Recurante(int n);
long long int puissanceN_Recurante(int a, int n);
int PGCD_Recurant(int a, int b);

int somme(int n);
long long int puissanceN(int a, int n);
int PGCD(int a, int b);

#endif //RECURSIVITE_FONCTIONS_H
